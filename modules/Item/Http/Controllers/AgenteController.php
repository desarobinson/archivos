<?php

namespace Modules\Item\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Item\Models\Agente;
use Modules\Item\Http\Resources\AgenteCollection;
use Modules\Item\Http\Resources\AgenteResource;
use Modules\Item\Http\Requests\AgenteRequest;

class AgenteController extends Controller
{

    public function index()
    {
        return view('item::agentes.index');
    }


    public function columns()
    {
        return [
            'operacion' => 'Operacion',
            'banks' => 'Banco/Caja',
        ];
    }

    public function records(Request $request)
    {
        $records = Agente::where($request->column, 'like', "%{$request->value}%")
                            ->latest();

        return new AgenteCollection($records->paginate(config('tenant.items_per_page')));
    }
    public function tables() {

        $users = User::get(['id', 'name']);

        return compact('users');

    }

    public function record($id)
    {
        $record = Agente::findOrFail($id);

        return $record;
    }

    public function store(AgenteRequest $request)
    {
        $id = $request->input('id');
        $agente = Agente::firstOrNew(['id' => $id]);
        $agente->fill($request->all());
        $agente->save();


        return [
            'success' => true,
            'message' => ($id)?'Categoría editada con éxito':'Categoría registrada con éxito',
            'data' => $agente

        ];

    }

    public function destroy($id)
    {
        try {

            $agente = Agente::findOrFail($id);
            $agente->delete();

            return [
                'success' => true,
                'message' => 'Categoría eliminada con éxito'
            ];

        } catch (Exception $e) {

            return ($e->getCode() == '23000') ? ['success' => false,'message' => "La categoría esta siendo usada por otros registros, no puede eliminar"] : ['success' => false,'message' => "Error inesperado, no se pudo eliminar la categoría"];

        }

    }




}

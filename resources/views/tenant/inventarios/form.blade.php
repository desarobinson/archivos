@extends('tenant.layouts.app')

@section('content')
 
    <tenant-inventarios-form :purchase_order_id="{{ json_encode($purchase_order_id) }}"></tenant-inventarios-form>

@endsection